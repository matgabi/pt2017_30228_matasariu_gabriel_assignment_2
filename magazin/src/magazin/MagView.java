package magazin;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;

public class MagView extends JFrame {
	private JTextField duration = new JTextField(30);
	private JTextField nrQ = new JTextField(30);
	private JTextField maxService = new JTextField(30);
	private JTextField minService = new JTextField(30);
	private JTextField minArrInt = new JTextField(30);
	private JTextField maxArrInt = new JTextField(30);
	protected static JTextArea log = new JTextArea(30, 70);
	protected static JScrollPane scroll = new JScrollPane(log);
	private JButton start = new JButton("Start sim");
	private JButton draw = new JButton("draw");
	protected static JPanel view = new JPanel();
	private JTextField averageWaitingTime = new JTextField(30);
	private JTextField averageServiceTime = new JTextField(30);
	private JTextField peakHour = new JTextField(30);
	private JTextField emptyQueueTime = new JTextField(30);

	public MagView() {
		JPanel content = new JPanel();
		content.setLayout(new BoxLayout(content, BoxLayout.Y_AXIS));

		JPanel inout = new JPanel();
		inout.setLayout(new GridLayout(1, 2));
		JPanel in = new JPanel();
		in.setLayout(new GridLayout(7, 2));
		in.add(new JLabel("Duration : "));
		in.add(duration);
		in.add(new JLabel("Number of queues : "));
		in.add(nrQ);
		in.add(new JLabel("Max service : "));
		in.add(maxService);
		in.add(new JLabel("Min service : "));
		in.add(minService);
		in.add(new JLabel("Min interval : "));
		in.add(minArrInt);
		in.add(new JLabel("Max interval : "));
		in.add(maxArrInt);
		in.add(start);
		in.add(draw);
		
		JPanel out = new JPanel();
		out.setLayout(new GridLayout(7, 2));
		out.add(new JLabel("Average waiting time: "));
		out.add(averageWaitingTime);
		out.add(new JLabel("Average service time: "));
		out.add(averageServiceTime);
		out.add(new JLabel("Empty queue time: "));
		out.add(emptyQueueTime);
		out.add(new JLabel("Peak hour : "));
		out.add(peakHour);
		this.averageServiceTime.setEditable(false);
		this.averageWaitingTime.setEditable(false);
		this.peakHour.setEditable(false);
		this.emptyQueueTime.setEditable(false);
		
		inout.add(in);
		inout.add(out);

		JPanel sim = new JPanel();
		sim.setLayout(new GridLayout(1, 2));
		sim.add(view);

		log.setEditable(false);
		sim.add(scroll);
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

		content.add(inout);
		content.add(sim);

		duration.setText("30");
		minService.setText("1");
		maxService.setText("4");
		minArrInt.setText("3");
		maxArrInt.setText("7");
		nrQ.setText("5");

		this.setContentPane(content);
		this.setVisible(true);
		this.pack();
		this.setExtendedState(JFrame.MAXIMIZED_BOTH);
		// this.setSize(400,600);
		this.setTitle("Queue simulator");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);

	}

	public synchronized void redrawView(int nr, LinkedList<Client>[] queues) { //functie care imi redeseneaza cozile in functie de numarul de clienti din aceasta
		view.removeAll();
		view.setLayout(new GridLayout(nr, 1));

		for (int i = 0; i < nr; i++) {
			int ocupat = 0;
			JPanel y = new JPanel();
			JLabel x = new JLabel();
			y.setLayout(new GridLayout(1, queues[i].isEmpty() ? 1 : queues[i].size() + 1));
			ImageIcon img = new ImageIcon("E:\\Java\\magazin\\cashregister.png");
			Image image = img.getImage();
			Image newimg = image.getScaledInstance(50, 50, java.awt.Image.SCALE_SMOOTH);
			img = new ImageIcon(newimg);
			x.setIcon(img);
			y.add(x);
			ocupat++;
			for (Client c : queues[i]) {
				y.add(c.getId());
			}

			view.add(y);

		}
		view.revalidate();

	}

	public void addStartListener(ActionListener l) {
		start.addActionListener(l);
	}

	public JTextArea getLog() {
		return log;
	}

	public int getQueNr() {
		int nr = 1;
		try{
			nr = !nrQ.getText().equals("") ? new Integer(nrQ.getText()) : 1;
		}
		catch(Exception e){
			
		}
		return nr;
	}

	public int getDuration() {
		return new Integer(duration.getText());
	}

	public int getMinService() {
		return new Integer(minService.getText());
	}

	public int getMaxService() {
		return new Integer(maxService.getText());
	}

	public int getMinArrInt() {
		return new Integer(minArrInt.getText());
	}

	public int getMaxArrInt() {
		return new Integer(maxArrInt.getText());
	}
	
	public void setAverageWT(String text){
		this.averageWaitingTime.setText(text);
	}
	
	public void setAverageST(String text){
		this.averageServiceTime.setText(text);
	}

	public void setPeakHour(String text){
		this.peakHour.setText(text);
	}
	
	public void setEmptyQueueTime(String text){
		this.emptyQueueTime.setText(text);
	}
	
	public static void main(String[] args) {
		MagView proces = new MagView();
	}
}
