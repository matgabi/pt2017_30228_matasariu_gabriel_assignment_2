package magazin;

import java.util.LinkedList;
import java.util.concurrent.ThreadLocalRandom;

public class ClientGenerator implements Runnable {
	private LinkedList<Client>[] queues; //preiau cozile deja existente 
	// queues[0] = new LinkedList<Integer>();
	private boolean running = true; //variabila booleana care controleaza executia threadului
	private int nrQ;  //conditiile necesare introduse in interfata pt generarea unui nou client
	private int minService;
	private int maxService;
	private int minArrInt;
	private int maxArrInt;

	public ClientGenerator(LinkedList[] queues, int nr, int[] data) {
		this.queues = queues; //primesc cozile din model
		nrQ = nr;
		minService = data[0]; //vectorul data cotine restrictiile pt generarea unui nou client
		maxService = data[1];
		minArrInt = data[2];
		maxArrInt = data[3];
	}

	public void setRunning(boolean running) { //opreste sau porneste threadul
		this.running = running;
	}

	@Override
	public synchronized void run() { //thread de generrare client
		while (running) {
			int rand = ThreadLocalRandom.current().nextInt(minArrInt, maxArrInt + 1); //genereaz aleator timpul de intrare al clientului
			int wtime = ThreadLocalRandom.current().nextInt(minService, maxService + 1);//generez aleator timpul de service pt fiecare client
			int min = queues[0].size(); 
			int pos = 0;
			for (int i = 1; i < nrQ; i++)
				if (queues[i].size() < min) {
					min = queues[i].size(); //verific ce coada are cei mai putini clienti si in aceasta coada voi introduce noul client
					pos = i;
				}
			try {
				Thread.sleep(rand * 100); //astept rand milisecunde pana bag noul client in coada
			} catch (InterruptedException e) {
				running = false;
			} catch (Exception e) {
				running = false; //in caz de eroare opresc executia threadului
			}
			try {
				Client newClient = new Client("client " + MagModel.nrC++, wtime, Clock.clock); 
				MagView.log.getDocument().insertString(0,
						newClient.getName() + " entered que " + pos + " at " + newClient.getEnterTime() + "s\n", null);
				MagView.log.update(MagView.log.getGraphics());
				MagView.scroll.revalidate(); //scriu in log ul din view datele despre noul client
				queues[pos].addLast(newClient); //introduc noul client
			} catch (Exception e) {
				running = false; //in caz de eroare opresc executia threadului
			}
		}
	}
}
