package magazin;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.*;

public class MagController { //controllerul aplicatiei
	private static MagView view;
	private static MagModel model;

	public MagController(MagView view, MagModel model) {
		this.view = view;
		this.model = model;
		view.addStartListener(new StartListener());
	}
 
	public static void redraw() { //apeleaza functia de redesenare
		view.redrawView(view.getQueNr(), model.getQueues());
		view.revalidate();
		view.view.update(view.view.getGraphics());
	}
	
	public static int getDuration(){ //preia durata simularii
		return model.getDuration();
	}
	
	public static void processWaitingTime(int waitingTime){ //adaug timpul de asteptare al noului client la suma timpilor de asteptare
		model.processWaitingTime(waitingTime);
	}
	
	public static void processServiceTime(int serviceTime){//adaug timpul de service la...
		model.processServiceTime(serviceTime);
	}
	
	public static void processClient(){
		model.processClient(); //un nou client a fost procesat de aplicatie
	}

	class StartListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			model.setNrQ(view.getQueNr());
			model.resetNrC();
			model.initQueues();
			model.setDuration(view.getDuration());
			model.setMaxArrInt(view.getMaxArrInt());
			model.setMinArrInt(view.getMinArrInt());
			model.setMaxService(view.getMaxService());
			model.setMinService(view.getMinService());
			

			MagView.log.setText(null);
			view.redrawView(view.getQueNr(), model.getQueues());
			view.revalidate();
			view.update(view.getGraphics());

			model.startSim();
			view.setAverageWT(model.getAverageWT()+"");
			view.setAverageST(model.getAverageST()+"");
			view.setEmptyQueueTime(Clock.emptyClk+"");
			view.setPeakHour(Clock.peakHour+"");

		}
	}

	public static void main(String[] args) {
		MagModel model = new MagModel();
		MagView view = new MagView();
		MagController contr = new MagController(view, model);
	}
}
