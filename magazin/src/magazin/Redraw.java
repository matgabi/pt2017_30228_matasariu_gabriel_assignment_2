package magazin;

public class Redraw implements Runnable {
//threadul de redesenare al cozilor
	private boolean running = true;
	public void stopRedraw(){
		this.running = false;
	}
	public void run(){
		while(running){
			try{
				
				Thread.sleep(50);
				MagController.redraw();
				
			}catch(Exception e){
				running = false;
			}
		}
	}
	
}
