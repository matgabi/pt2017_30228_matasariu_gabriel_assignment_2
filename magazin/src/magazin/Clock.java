package magazin;

import java.util.LinkedList;
//threadul de clock
public class Clock implements Runnable{
	private LinkedList<Client>[] queues; //primeste listele de clienti
	public static int clock; //clock ul general
	public static int emptyClk; //cat timp a fost goala coada
	public static int peakHour; //ora cu numar mare de clienti 
	private int maxClients;  //numar maxim de clienti pt a seta peakHour
	private boolean running = true;
	
	public Clock(LinkedList<Client>[] queues){ //constructor
		clock = 0;
		this.queues = queues;
	}
	
	public void run(){
		while(running){ //incrementeaza clockul,peak hour,si empty service time in functie de conditii
			try{
				Thread.sleep(950);
				clock++;
				if(emptyQueue())
					emptyClk++;
				countClients();
			}catch(Exception e){
				running = false;
			}
		}
	}
	
	private synchronized void countClients(){ //numara clientii din toate cozile
		int clients = 0;
		for(LinkedList<Client> queue : queues)
			clients += queue.size();
		if(maxClients < clients){
			maxClients = clients;
			peakHour = clock;
		}
	}
	
	private boolean emptyQueue(){ //testeaza daca una din cozi e goala
		for(LinkedList<Client> queue : queues)
			if(queue.isEmpty())
				return true;
		return false;
			
	}
}
