package magazin;

import java.util.LinkedList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class MagModel {
	public static int nrC = 0; //numarul de clienti de la inceputul simularii
	private int processedClients; //numarul de clienti procesati
	private int waitingTime;//suma timpilor de asteptare
	private int serviceTime;//suma timpilor de service
	private int nrQ; //numarul de cozie
	private int duration; //durata simularii
	private int minService; //timpul minim de service
	private int maxService; //timpul maxim de service
	private int minArrInt; //timpul minim de generare al unui nou client
	private int maxArrInt; //timpul maxim de generare al unui nou client
	
	private static LinkedList<Client>[] queues; //cozile de clienti

	public LinkedList<Client>[] getQueues() { //getter cozi
		return queues;
	}
	
	public void processWaitingTime(int waitingTime){ //aduna timpul de asteptare la suma totala
		this.waitingTime += waitingTime;
	}
	
	public void processServiceTime(int serviceTime){ //aduna timpul de servicela suma totala 
		this.serviceTime += serviceTime;
	}
	
	public void processClient(){
		this.processedClients++; //incrementez numarul de clienti procesati
	}

	public void setNrQ(int nr) { //setez numarul de cozi
		this.nrQ = nr;
		
	}
	
	public void initQueues(){ //initializeaza cozile
		queues = new LinkedList[nrQ];
		for (int i = 0; i < nrQ; i++) {
			queues[i] = new LinkedList<Client>();
			queues[i].addFirst(new Client("client " + nrC++, 6, 0));
		}
	}

	public void setDuration(int duration) { //seteaza durata simularii
		this.duration = duration;
	}

	public void setMinService(int minService) { //setter
		this.minService = minService;
	}

	public void setMaxService(int maxService) {//setter
		this.maxService = maxService;
	}

	public void setMinArrInt(int minArrInt) { //setter
		this.minArrInt = minArrInt;
	}

	public void setMaxArrInt(int maxArrInt) { //setter
		this.maxArrInt = maxArrInt;
	}
	
	public int getDuration(){ //getter
		return this.duration;
	}
	
	public double getAverageWT(){ //getter
		Double avg = new Double(this.waitingTime+"");
		return avg / this.processedClients;
	}
	
	public double getAverageST(){ //getter
		Double avg = new Double(this.serviceTime+"");
		return avg/this.processedClients;
	}
	
	public void resetNrC(){ //reseteaza indcii pentru o noua rulare
		this.nrC = 0;
		this.waitingTime = 0;
		this.processedClients = 0;
		this.serviceTime = 0;
	}

	public void startSim() {
		//incepe simularea
		MagView.log.removeAll();//sterg tot din log ul din view
		int[] data = { minService, maxService, minArrInt, maxArrInt }; //vectorul trimis spre client generator cu restrictiile necesare
		ExecutorService executor = Executors.newFixedThreadPool(nrQ + 3); //excutorul de threaduri
		for (int i = 0; i < nrQ; i++) {
			Runnable worker = new WorkerThread("" + i, queues[i], "queue " + i);
			executor.execute(worker); //generez nrQ threaduri de procesare a clientilor
		}
		ClientGenerator gen = new ClientGenerator(queues, nrQ, data);
		Runnable clientGen = gen;
		Redraw red = new Redraw();
		Runnable redGen = red;
		executor.execute(redGen); //generez threadul de redesenare
		executor.execute(clientGen); //generez threadul de generare a unui nou client
		Runnable clk = new Clock(queues);
		executor.execute(clk); //generez threadul clk care functioneaza ca un "ceas" artificial pentru simulator
		executor.shutdown();
		try {
			executor.awaitTermination(duration, TimeUnit.SECONDS); //termina toate threaduril cand timpul de simulare a trecut
		} catch (Exception e) {

		}	
		gen.setRunning(false); //in caz ca au aparut erori la oprire opresc generatorul de clienti
		red.stopRedraw(); //in caz ca au aparut erori la oprire opresc threadul de redesenare
		executor.shutdownNow(); //inchid toate threadurile
		while (!executor.isTerminated()) { //daca nu s-au terminat threadurile nu fa nimic
		}
		System.out.println("Total clienti : "+ this.processedClients);
		System.out.println("Total st : "+ this.serviceTime);
		System.out.println("Total wt : "+ this.waitingTime);
		System.out.println("Finished all threads");
	}
}
