package magazin;

import java.util.concurrent.ThreadLocalRandom;
import java.util.*;

public class WorkerThread implements Runnable { //threadul care imi proceseaza primul client din fiecare coada
	private boolean running = true; //variabila booleana care controleaza executia threadului
	private String command; 
	private LinkedList<Client> queue; //preia coada care trebuie procesat la momentul X

	public WorkerThread(String s, LinkedList<Client> queue, String name) { //constructor
		Thread.currentThread().setName(name);
		this.command = s;
		this.queue = queue;
	}

	@Override
	public synchronized void run() {
		while (running) { //atata timp cat threadul ar trebui sa ruleze

			if (queue.isEmpty()) { //in cazul in care coada e goala threadul trece pe sleep timp de o secunda
				// processCommand(0);
				try {
					Thread.currentThread().sleep(1);
				} catch (Exception e) {
					System.out.println("Exception " + Thread.currentThread().getName());
					running = false;
				}
			} else { //daca coada nu e goala
				// System.out.println("Queue" +
				// Thread.currentThread().getName().split("-")[3]+" new client"
				// + " at the desk. Service time : " +
				// queue.getFirst().getServiceTime() );
				processCommand(queue.getFirst()); //procesez primul client din coada
				System.out.println(queue.getFirst()); //afisez informatiile despre clientul procesat atunci cand acesta a iesit din coada
				try {
					MagView.log.getDocument().insertString(0, queue.getFirst().toString(), null);
					MagView.log.update(MagView.log.getGraphics());
					MagView.scroll.revalidate();
					//scriu in log ul din view aceleasi informatii
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				//transmit in controller informatii despre timpii de asteptare respectiv de service time 
				MagController.processServiceTime(queue.getFirst().getServiceTime());
				if(queue.getFirst().getExitTime()!=0)
					MagController.processWaitingTime(queue.getFirst().getExitTime() - queue.getFirst().getEnterTime());
				else
					MagController.processWaitingTime(MagController.getDuration() - queue.getFirst().getEnterTime());
				queue.removeFirst();  //scot clientul din lista
				MagController.processClient(); //incrementez numarul de clienti procesati
			//	MagController.redraw();
			}
		}
	}

	private synchronized void processCommand(Client rand) {
		try { //proceseaza comanda
			Thread.sleep(rand.getServiceTime() * 1000);
			rand.setExitTime(Clock.clock); //setez timpul de exit

		} catch (InterruptedException e) {
			running = false;
		}
	}

	@Override
	public String toString() {
		return this.command;
	}
}
