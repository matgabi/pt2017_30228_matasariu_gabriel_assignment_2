package magazin;
import java.util.*;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class Client {
	private int serviceTime; //service time pt fiecare client
	private int enterTime; //timp de intrare
	private int exitTime; //timp de iesire
	private String name; // nume client
	private JLabel id; //un jlabel care va contine imaginea ce va fi afisataa in interfata
	
	public Client(String name,int serviceTime,int enterTime){ //constructor
		this.name = name;  //set nume
		this.serviceTime = serviceTime; //set service
		this.enterTime = enterTime; //set timp intrare
		id = new JLabel();
		ImageIcon img = new ImageIcon("E:\\Java\\magazin\\customer.png");
		Image image = img.getImage();
		Image newimg = image.getScaledInstance(50, 50, java.awt.Image.SCALE_SMOOTH);
		img = new ImageIcon(newimg);
		id.setIcon(img); //setez imaginea clientului
	}
	
	public JLabel getId(){
		return this.id; //getter pt "imaginea clientului"
	}
	
	public int getServiceTime(){
		return this.serviceTime; //getter service time
	}
	
	public int getEnterTime(){
		return this.enterTime; //getter enter time
	}
	
	public int getExitTime(){
		return this.exitTime; //getter exit time
	}
	
	public void setExitTime(int exitTime){
		this.exitTime = exitTime; //setter exit time
	}
	
	public String getName(){
		return this.name; //setter nume
	}
	
	public String toString(){ //afisare "citibila" a unui obiect de tip client
 		return name + "---->        enter : "+enterTime + "s       service : " + serviceTime + "s         exited : "+exitTime+"s\n";
	}
}
